# Build Stage
FROM rust:1.42 AS builder
WORKDIR /usr/src/
RUN rustup target add x86_64-unknown-linux-musl

RUN USER=root cargo new autodiscover
WORKDIR /usr/src/autodiscover
COPY Cargo.toml Cargo.lock ./
RUN cargo build --release

COPY ./src ./src
RUN cargo install --target x86_64-unknown-linux-musl --path .

# Bundle Stage
FROM scratch
COPY --from=builder /usr/local/cargo/bin/autodiscover .
USER 1000
ENTRYPOINT ["./autodiscover"]
