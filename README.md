# Autodiscover

```
USAGE:
    autodiscover [OPTIONS] --domain <domain> --imap-host <imap-host> --smtp-host <smtp-host>

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information

OPTIONS:
    -d, --domain <domain>          Your default domain [env: DOMAIN=]
        --http-port <http-port>    HTTP port [env: HTTP_PORT=]  [default: 8080]
    -i, --imap-host <imap-host>    Your IMAP host [env: IMAP_HOST=]
        --imap-port <imap-port>    Your secure IMAP port [env: IMAP_PORT=]  [default: 993]
    -s, --smtp-host <smtp-host>    Your SMTP host [env: SMTP_HOST=]
        --smtp-port <smtp-port>    Your secure SMTP port [env: SMTP_PORT=]  [default: 465]
```
