use autodiscover::messages::ms_oxdscli::{AutodiscoverRequest, AutodiscoverResponse};
use autodiscover::messages::autoconfig::ClientConfig;
use yaserde::ser::to_string;

fn main() {
    let d = AutodiscoverResponse::default()
        .with_user(String::from("user@domain.org"))
        .with_imap_server(
            String::from("imap.foo.com"),
            993,
            String::from("user@domain.org"),
            String::from("domain.org"),
        )
        .with_smtp_server(
            String::from("smtp.foo.com"),
            465,
            String::from("user@domain.org"),
            String::from("domain.org"),
        );

    println!("{}", to_string(&d).unwrap());

    let r = AutodiscoverRequest::default()
        .with_email(String::from("user@domain.org"));

    println!("{}", to_string(&r).unwrap());

    let c = ClientConfig::default()
        .with_display_name(Some(String::from("Tenoco.net")))
        .with_domain(String::from("tenoco.net"))
        .with_imap_server(
            String::from("imap.tenoco.net"),
            993,
            String::from("%EMAILADDRESS%"),
        )
        .with_smtp_server(
            String::from("smtp.tenoco.net"),
            587,
            String::from("%EMAILADDRESS%"),
        );

    println!("{}", to_string(&c).unwrap());
}
