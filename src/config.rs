use structopt::StructOpt;

#[derive(Debug, StructOpt, Clone)]
#[structopt(name = "autodiscover")]
pub struct Opt {
    /// Your default domain
    #[structopt(short, long, env)]
    pub domain: String,

    /// Your IMAP host
    #[structopt(short, long, env)]
    pub imap_host: String,

    /// Your SMTP host
    #[structopt(short, long, env)]
    pub smtp_host: String,

    /// Your secure IMAP port
    #[structopt(long, env, default_value = "993")]
    pub imap_port: u16,

    /// Your secure SMTP port
    #[structopt(long, env, default_value = "465")]
    pub smtp_port: u16,

    /// HTTP port
    #[structopt(long, env, default_value = "8080")]
    pub http_port: u16,

    /// Name of the provider
    #[structopt(long, env)]
    pub display_name: Option<String>,

    /// Short name of the provider
    #[structopt(long, env)]
    pub display_short_name: Option<String>,
}
