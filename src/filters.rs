use yaserde::YaDeserialize;
use yaserde::de::from_reader;
use bytes::buf::ext::BufExt;
use bytes::Buf;
use warp::Filter;
use mime::Mime;

type BoxError = Box<dyn std::error::Error + Send + Sync>;

fn decode_xml<B: Buf, T: YaDeserialize>(buf: B) -> Result<T, BoxError> {
    from_reader(buf.reader()).map_err(Into::into)
}

pub fn parse_xml<T: YaDeserialize + Send>() -> impl Filter<Extract = (T,), Error = warp::reject::Rejection> + Copy {
    warp::header::optional("Content-Type")
        .and_then(|content_type: Option<String>| async move {
            let content_type: Option<Mime> = content_type
                .and_then(|s| s.parse().ok());

            if let Some(ct) = content_type {
                match (ct.type_(), ct.subtype()) {
                    (mime::APPLICATION, mime::XML) => Ok(()),
                    (mime::TEXT, mime::XML) => Ok(()),
                    _ => Err(warp::reject::reject()),
                }
            } else {
                Ok(())
            }
        })
        .and(warp::body::aggregate())
        .and_then(|_, buf| async move {
            decode_xml(buf)
                .map_err(Into::into)
                .map_err(|err: BoxError| {
                    println!("{:?}", err);
                    warp::reject::reject()
                })
        })
}
