#[macro_use] extern crate yaserde_derive;

use std::env;
use std::net::Ipv6Addr;
use std::sync::Arc;
use structopt::StructOpt;
use warp::Filter;

mod config;
mod filters;
pub mod messages;

use crate::messages::ms_oxdscli::AutodiscoverResponse;
use crate::messages::autoconfig::ClientConfig;

#[tokio::main]
async fn main() {
    if env::var_os("RUST_LOG").is_none() {
        // Set `RUST_LOG=autodiscover=debug` to see debug logs,
        // this only shows access logs.
        env::set_var("RUST_LOG", "autodiscover=info");
    }
    pretty_env_logger::init();

    let opt = config::Opt::from_args();
    let http_port = opt.http_port.clone();
    let opt = Arc::new(opt);
    let with_opt = warp::any().map(move || opt.clone());

    let autoconfig_http = warp::path!("mail" / "config-v1.1.xml")
        .and(warp::get())
        .and(with_opt.clone())
        .and_then(|opt: Arc<config::Opt>| async move{
            let mut d = ClientConfig::default()
                .with_domain(opt.domain.clone())
                .with_imap_server(
                    opt.imap_host.clone(),
                    opt.imap_port.clone(),
                    String::from("%EMAILADDRESS%"),
                )
                .with_smtp_server(
                    opt.smtp_host.clone(),
                    opt.smtp_port.clone(),
                    String::from("%EMAILADDRESS%"),
                );

            if let Some(ref dn) = opt.display_name {
                d = d.with_display_name(dn.clone());
            }
            if let Some(ref dsn) = opt.display_short_name {
                d = d.with_display_short_name(dsn.clone());
            }

            yaserde::ser::to_string(&d).map_err(|_| warp::reject())
        })
        .with(warp::reply::with::header("Content-Type", mime::TEXT_XML.as_ref()));

    let autodiscover_ms_ascmd = warp::path!("Autodiscover" / "Autodiscover.xml")
        .or(warp::path!("autodiscover" / "autodiscover.xml"))
        .unify()
        .and(warp::post())
        .and(filters::parse_xml())
        .map(|req: messages::ms_ascmd::AutodiscoverRequest| req.email());

    let autodiscover_ms_oxdscli = warp::path!("Autodiscover" / "Autodiscover.xml")
        .or(warp::path!("autodiscover" / "autodiscover.xml"))
        .unify()
        .and(warp::post())
        .and(filters::parse_xml())
        .map(|req: messages::ms_oxdscli::AutodiscoverRequest| req.email().unwrap()); // TODO: error handling

    let autodiscover_http = autodiscover_ms_ascmd.or(autodiscover_ms_oxdscli)
        .unify()
        .and(with_opt)
        .and_then(|email: String, opt: Arc<config::Opt>| async move {
            let d = AutodiscoverResponse::default()
                .with_user(email.clone())
                .with_imap_server(
                    opt.imap_host.clone(),
                    opt.imap_port.clone(),
                    email.clone(),
                    opt.domain.clone(),
                )
                .with_smtp_server(
                    opt.smtp_host.clone(),
                    opt.smtp_port.clone(),
                    email.clone(),
                    opt.domain.clone(),
                );

            yaserde::ser::to_string(&d).map_err(|_| warp::reject())
        })
        .with(warp::reply::with::header("Content-Type", mime::TEXT_XML.as_ref()));

    let health = warp::path!("health")
        .map(warp::reply);

    let api = health.or(autoconfig_http).or(autodiscover_http)
        .with(warp::log("autodiscover"));

    warp::serve(api)
        .run((Ipv6Addr::UNSPECIFIED, http_port))
        .await;
}
