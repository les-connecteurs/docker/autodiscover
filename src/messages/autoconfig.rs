use std::io::{Read, Write};
use yaserde::{YaSerialize, YaDeserialize};

#[derive(Debug, YaSerialize, YaDeserialize, Default)]
#[yaserde(root = "clientConfig")]
pub struct ClientConfig {
    #[yaserde(attribute)]
    version: ConfigVersion,

    #[yaserde(rename = "emailProvider")]
    email_provider: EmailProvider,
}

impl ClientConfig {
    pub fn with_domain(mut self, domain: String) -> Self {
        if self.email_provider.id == "" {
            self.email_provider.id = domain.clone();
        }

        self.email_provider.domains.push(domain);
        self
    }

    pub fn with_display_name(mut self, name: String) -> Self {
        self.email_provider.display_short_name = Some(name);
        self
    }

    pub fn with_display_short_name(mut self, name: String) -> Self {
        self.email_provider.display_short_name = Some(name);
        self
    }

    pub fn with_imap_server(mut self, host: String, port: u16, username: String) -> Self {
        let socket_type = match port {
            143 => SocketType::StartTLS,
            993 => SocketType::SSL,
            _ => SocketType::StartTLS,
        };

        let i = IncomingServer {
            _type: IncomingServerType::IMAP,
            hostname: host,
            port: port,
            username: Some(username),
            socket_type,
            authentication: Authentication::PasswordCleartext,
        };

        self.email_provider.incoming_servers.push(i);
        self
    }

    pub fn with_smtp_server(mut self, host: String, port: u16, username: String) -> Self {
        let socket_type = match port {
            465 => SocketType::SSL,
            587 => SocketType::StartTLS,
            _ => SocketType::StartTLS,
        };

        let i = OutgoingServer {
            _type: OutgoingServerType::SMTP,
            hostname: host,
            port: port,
            username: Some(username),
            socket_type,
            authentication: Authentication::PasswordCleartext,
            restriction: None,
            add_this_server: true,
            use_global_preferred_server: false,
        };

        self.email_provider.outgoing_servers.push(i);
        self
    }
}

#[derive(Debug, YaSerialize, YaDeserialize)]
enum ConfigVersion {
    #[yaserde(rename = "1.1")]
    V1_1,
}

impl Default for ConfigVersion {
    fn default() -> Self {
        Self::V1_1
    }
}

#[derive(Debug, YaSerialize, YaDeserialize, Default)]
struct EmailProvider {
    #[yaserde(attribute)]
    id: String,

    #[yaserde(rename = "domain")]
    domains: Vec<String>,

    #[yaserde(rename = "displayName")]
    display_name: Option<String>,

    #[yaserde(rename = "displayShortName")]
    display_short_name: Option<String>,

    #[yaserde(rename = "incomingServer")]
    incoming_servers: Vec<IncomingServer>,

    #[yaserde(rename = "outgoingServer")]
    outgoing_servers: Vec<OutgoingServer>,
}

#[derive(Debug, YaSerialize, YaDeserialize)]
enum IncomingServerType {
    #[yaserde(rename = "imap")]
    IMAP,
    #[yaserde(rename = "pop3")]
    POP3,
    #[yaserde(rename = "nntp")]
    NNTP,
    #[yaserde(rename = "exchange")]
    Exchange,
}

impl Default for IncomingServerType {
    fn default() -> Self {
        unimplemented!()
    }
}

#[derive(Debug, YaSerialize, YaDeserialize)]
enum SocketType {
    #[yaserde(rename = "plain")]
    Plain,
    SSL,
    #[yaserde(rename = "STARTTLS")]
    StartTLS,
}

impl Default for SocketType {
    fn default() -> Self {
        unimplemented!()
    }
}

#[derive(Debug, YaSerialize, YaDeserialize)]
enum Authentication {
    #[yaserde(rename = "password-cleartext")]
    PasswordCleartext,
    #[yaserde(rename = "password-encrypted")]
    PasswordEncrypted,
    NTLM,
    GSSAPI,
    #[yaserde(rename = "client-IP-address")]
    ClientIPAddress,
    #[yaserde(rename = "TLS-client-cert")]
    TLSClientCert,
    #[yaserde(rename = "none")]
    None,
}

impl Default for Authentication {
    fn default() -> Self {
        unimplemented!()
    }
}

#[derive(Debug, YaSerialize, YaDeserialize, Default)]
#[yaserde(root = "incomingServer")]
struct IncomingServer {
    #[yaserde(rename = "type", attribute)]
    _type: IncomingServerType,

    hostname: String,

    port: u16,

    #[yaserde(rename = "socketType")]
    socket_type: SocketType,

    username: Option<String>,

    authentication: Authentication,
}

#[derive(Debug, YaSerialize, YaDeserialize)]
enum OutgoingServerType {
    #[yaserde(rename = "smtp")]
    SMTP,
}

impl Default for OutgoingServerType {
    fn default() -> Self {
        Self::SMTP
    }
}

fn yes() -> bool { true }
fn no() -> bool { false }

#[derive(Debug, YaSerialize, YaDeserialize)]
enum OutgoingServerRestriction {
    #[yaserde(rename = "client-IP-address")]
    ClientIPAddress,
}

impl Default for OutgoingServerRestriction {
    fn default() -> Self {
        unimplemented!()
    }
}

#[derive(Debug, YaSerialize, YaDeserialize, Default)]
#[yaserde(root = "outgoingServer")]
struct OutgoingServer {
    #[yaserde(rename = "type", attribute)]
    _type: OutgoingServerType,

    hostname: String,

    port: u16,

    #[yaserde(rename = "socketType")]
    socket_type: SocketType,

    username: Option<String>,

    authentication: Authentication,

    restriction: Option<OutgoingServerRestriction>,

    #[yaserde(rename = "add_this_server", default = "yes")]
    add_this_server: bool,

    #[yaserde(rename = "useGlobalPreferredServer", default = "no")]
    use_global_preferred_server: bool,
}
