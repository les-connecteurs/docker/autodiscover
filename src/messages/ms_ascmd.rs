use std::io::{Read, Write};
use yaserde::{YaDeserialize, YaSerialize};

#[derive(Debug, YaDeserialize, YaSerialize, Default)]
#[yaserde(
    root = "Autodiscover",
    namespace = "ns: http://schemas.microsoft.com/exchange/autodiscover/mobilesync/requestschema/2006",
    prefix = "ns",
    default_namespace = "ns",
)]
pub struct AutodiscoverRequest {
    #[yaserde(rename = "Request")]
    request: Request,
}

impl AutodiscoverRequest {
    pub fn email(self) -> String {
        self.request.email_address
    }
}

#[derive(Debug, YaDeserialize, YaSerialize, Default)]
struct Request {
    #[yaserde(rename = "EMailAddress", prefix = "ns")]
    email_address: String,

    #[yaserde(rename = "AcceptableResponseSchema", prefix = "ns")]
    acceptable_response_schema: AcceptableResponseSchema,
}

#[derive(Debug, YaDeserialize, YaSerialize)]
enum AcceptableResponseSchema {
    #[yaserde(rename = "http://schemas.microsoft.com/exchange/autodiscover/mobilesync/responseschema/2006")]
    Schema2006,
}

impl Default for AcceptableResponseSchema {
    fn default() -> Self {
        Self::Schema2006
    }
}
