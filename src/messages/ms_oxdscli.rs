use std::io::{Read, Write};
use yaserde::{YaDeserialize, YaSerialize};

/// Autodiscover responses consist of a single Autodiscover element, which contains configuration
/// information for the user's mailbox within its child elements.
#[derive(Debug, YaDeserialize, YaSerialize, Default)]
#[yaserde(
    root = "Autodiscover",
    namespace = "nsa: http://schemas.microsoft.com/exchange/autodiscover/responseschema/2006",
    prefix = "nsa",
    default_namespace = "nsa",
)]
pub struct AutodiscoverResponse {
    #[yaserde(rename = "Response")]
    response: Response,
}

impl AutodiscoverResponse {
    /// Fills user-related fields. Expects a valid email.
    pub fn with_user(mut self, email: String) -> Self {
        self.response.user.display_name = email.clone();
        self.response.user.auto_discover_smtp_address = email.clone();
        self.response.user.deployment_id = String::from("00000000-0000-0000-0000-000000000000");

        // Derive the DN from the email
        // This will crash if the email is invalid.
        let parts: Vec<_> = email.splitn(2, '@').collect();
        let user = parts[0];
        let domain = parts[1];
        let mut parts: Vec<_> = domain.rsplit('.').map(|part| format!("/dc={}", part)).collect();
        parts.push(format!("/ou=users/cn={}", user));
        self.response.user.legacy_dn = parts.join("");

        self
    }

    pub fn with_smtp_server(mut self, host: String, port: u16, email: String, domain: String) -> Self {
        let p = Protocol::default()
            .with_type(ProtocolType::SMTP)
            .with_domain(domain)
            .with_server(host, port)
            .with_login(email)
            .with_misc()
            .fill_encryption();
        self.response.account.protocols.push(p);
        self
    }

    pub fn with_imap_server(mut self, host: String, port: u16, email: String, domain: String) -> Self {
        let p = Protocol::default()
            .with_type(ProtocolType::IMAP)
            .with_domain(domain)
            .with_server(host, port)
            .with_login(email)
            .with_misc()
            .fill_encryption();
        self.response.account.protocols.push(p);
        self
    }
}

/// The Response element contains the response from the Autodiscover server that includes a list of
/// URLs that are used to establish a connection with web services.
#[derive(Debug, YaDeserialize, YaSerialize, Default)]
#[yaserde(
    root = "Response",
    namespace = "nsb: http://schemas.microsoft.com/exchange/autodiscover/outlook/responseschema/2006a",
    prefix = "nsb",
    default_namespace = "nsb",
)]
struct Response {
    #[yaserde(prefix = "nsb", rename = "User")]
    user: User,

    #[yaserde(prefix = "nsb", rename = "Account")]
    account: Account,
}

/// The User element and its child elements provide user-specific information. Servers MUST include
/// this element if the server does not need to redirect the request and encounters no errors.
#[derive(Debug, YaDeserialize, YaSerialize, Default)]
#[yaserde(
    root = "Response",
    namespace = "nsb: http://schemas.microsoft.com/exchange/autodiscover/outlook/responseschema/2006a",
    prefix = "nsb",
    default_namespace = "nsb",
)]
struct User {
    /// The AutoDiscoverSMTPAddress element represents the user's primary Simple Mail Transfer
    /// Protocol (SMTP) address. It is a required child element of the User element. This is the
    /// email address that is used for the Autodiscover process. The AutoDiscoverSMTPAddress
    /// element returns the proxy address in lieu of the email address if a proxy address exists.
    #[yaserde(prefix = "nsb", rename = "AutoDiscoverSMTPAddress")]
    auto_discover_smtp_address: String,

    /// The DefaultABView element indicates the default view for the user's address book. If the
    /// Global Address List (GAL) is the default view, this element SHOULD NOT be present. If the
    /// Contacts folder in the user's mailbox is the default view, this element SHOULD be present
    /// with a value of "contacts".
    #[yaserde(prefix = "nsb", rename = "DefaultABView")]
    default_ab_view: Option<String>,

    /// The DeploymentId element is returned when the user is within a server forest. The returned
    /// value is the GUID identifier of the Active Directory forest in which the mailbox user
    /// account is contained.
    #[yaserde(prefix = "nsb", rename = "DeploymentId")]
    deployment_id: String,

    /// The DisplayName element represents the user's display name.
    #[yaserde(prefix = "nsb", rename = "DisplayName")]
    display_name: String,

    /// The LegacyDN element identifies a user's mailbox by DN. The LegacyDN is also known as the
    /// ESSDN, which is the naming scheme that defines the user.
    #[yaserde(prefix = "nsb", rename = "LegacyDN")]
    legacy_dn: String,
}

/// The AccountType element represents the account type.
#[derive(Debug, YaDeserialize, YaSerialize, PartialEq)]
enum AccountType {
    #[yaserde(rename = "email")]
    Email
}

impl Default for AccountType {
    fn default() -> Self {
        Self::Email
    }
}

/// The Action element provides information that is used to determine whether another Autodiscover
/// request is required to return the user configuration information.
#[derive(Debug, YaDeserialize, YaSerialize, PartialEq)]
enum Action {
    /// The Autodiscover server has returned configuration settings in the Protocol element.
    #[yaserde(rename = "settings")]
    Settings,

    /// The Autodiscover server has returned a RedirectAddr element.
    #[yaserde(rename = "redirectAddr")]
    RedirectAddr,

    /// The Autodiscover server has returned a RedirectUrl element.
    #[yaserde(rename = "redirectUrl")]
    RedirectUrl,
}

impl Default for Action {
    fn default() -> Self {
        Self::Settings
    }
}

#[derive(Debug, YaDeserialize, YaSerialize, PartialEq)]
enum Bool {
    True,
    False,
}

impl Default for Bool {
    fn default() -> Self {
        Self::False
    }
}


#[derive(Debug, YaDeserialize, YaSerialize, Default)]
#[yaserde(
    root = "Account",
    namespace = "nsb: http://schemas.microsoft.com/exchange/autodiscover/outlook/responseschema/2006a",
    prefix = "nsb",
    default_namespace = "nsb",
)]
struct Account {
    /// The AccountType element represents the account type.
    #[yaserde(prefix = "nsb", rename = "AccountType")]
    account_type: AccountType,

    /// The Action element provides information that is used to determine whether another
    /// Autodiscover request is required to return the user configuration information.
    #[yaserde(prefix = "nsb", rename = "Action")]
    action: Action,

    /// The MicrosoftOnline element specifies whether the user account is an online account.
    #[yaserde(prefix = "nsb", rename = "MicrosoftOnline")]
    microsoft_online: Bool,

    /// The ConsumerMailbox element specifies whether the user account is a consumer mailbox.
    #[yaserde(prefix = "nsb", rename = "ConsumerMailbox")]
    consumer_mailbox: Option<Bool>,

    /// The AlternativeMailbox element contains information that enables clients to open an
    /// additional mailbox.
    #[yaserde(prefix = "nsb", rename = "AlternativeMailbox")]
    alternative_mailbox: Option<AlternativeMailbox>,

    /// The Protocol element contains the configuration information for connecting a client to the
    /// server.
    #[yaserde(prefix = "nsb", rename = "Protocol")]
    protocols: Vec<Protocol>,

    #[yaserde(prefix = "nsb", rename = "PublicFolderInformation")]
    public_folder_information: Option<PublicFolderInformation>,
}

#[derive(Debug, YaDeserialize, YaSerialize, PartialEq)]
enum ProtocolType {
    EXCH,
    EXPR,
    EXHTTP,
    POP3,
    SMTP,
    IMAP,
    DAV,
    WEB,
}

impl Default for ProtocolType {
    fn default() -> Self {
        Self::EXCH
    }
}

#[derive(Debug, YaDeserialize, YaSerialize, PartialEq)]
enum Switch {
    #[yaserde(rename = "on")]
    On,
    #[yaserde(rename = "off")]
    Off,
}

impl Default for Switch {
    fn default() -> Self {
        Self::Off
    }
}

#[derive(Debug, YaDeserialize, YaSerialize, PartialEq)]
enum Encryption {
    /// No encryption is used.
    None,

    /// SSL encryption is used.
    SSL,

    /// TLS encryption is used.
    TLS,

    /// The most secure encryption that the client and server support is used.
    Auto,
}

impl Default for Encryption {
    fn default() -> Self {
        Self::Auto
    }
}

/// The Protocol element contains the configuration information for connecting a client to the
/// server.
#[derive(Debug, YaDeserialize, YaSerialize, Default)]
#[yaserde(
    root = "Protocol",
    namespace = "nsb: http://schemas.microsoft.com/exchange/autodiscover/outlook/responseschema/2006a",
    prefix = "nsb",
    default_namespace = "nsb",
)]
struct Protocol {

    /// The AD element specifies the Active Directory server used in conjunction with the mailbox.
    /// The element contains the FQDN of a Lightweight Directory Access Protocol (LDAP) server that
    /// the client can connect to for directory information.
    #[yaserde(prefix = "nsb", rename = "AD")]
    ad: Option<String>,

    /// The ASUrl element specifies the URL of the best endpoint instance of Availability web
    /// services for an email-enabled user, as specified in [MS-OXWAVLS].
    #[yaserde(prefix = "nsb", rename = "ASUrl")]
    as_url: Option<String>,

    /// The AddressBook element contains information that the client can use to connect to an NSPI
    /// server via Messaging Application Programming Interface (MAPI) Extensions for HTTP, as
    /// specified in [MSOXCMAPIHTTP], to retrieve address book information.
    #[yaserde(prefix = "nsb", rename = "AddressBook")]
    address_book: Option<NSPIInfo>,

    /// The AuthPackage element specifies the authentication method that is used when
    /// authenticating to the server that contains the user's mailbox. The AuthPackage element is
    /// used only when the Type element has a text value of "EXCH", "EXPR", or "EXHTTP".
    #[yaserde(prefix = "nsb", rename = "AuthPackage")]
    auth_package: Option<String>, // TODO: enum

    /// The AuthRequired element specifies whether authentication is required. The AuthRequired
    /// element is returned only when the Type element has a text value of "POP3".
    #[yaserde(prefix = "nsb", rename = "AuthRequired")]
    auth_required: Option<Switch>,

    /// The CertPrincipalName element specifies the SSL certificate principal name that is required
    /// to connect to the server by using SSL.
    ///
    /// If the CertPrincipalName element is not specified, the default value is "msstd:SERVER",
    /// where "SERVER" is the value that is specified in the Server element. For example, if
    /// "SERVER" is specified as "server.Contoso.com" and CertPrincipalName is left blank with SSL
    /// turned on, the default value of CertPrincipalName would be "msstd:server.Contoso.com".
    ///
    /// The CertPrincipalName element is returned only when the connection to the server is
    /// authenticated with SSL.
    #[yaserde(prefix = "nsb", rename = "CertPrincipalName")]
    cert_principal_name: Option<String>,

    /// The DomainName element specifies the user's domain. If no value is specified, the default
    /// value is the email address in user principal name (UPN) format.
    /// For example: <username>@<domain>.
    #[yaserde(prefix = "nsb", rename = "DomainName")]
    domain_name: Option<String>,

    /// The DomainRequired element contains a text value that indicates whether the domain is
    /// required for authentication.
    #[yaserde(prefix = "nsb", rename = "DomainRequired")]
    domain_required: Option<Switch>,

    /// The EcpUrl element is the base Exchange Control Panel (ECP) URL.
    #[yaserde(prefix = "nsb", rename = "EcpUrl")]
    ecp_url: Option<String>,

    // FIXME: There is a bug in yaserde where it can't accept names with dashes.
    // /// The EcpUrl-aggr element contains a value that, when appended to the value of the EcpUrl
    // /// element, results in a URL that can be used to access email aggregation settings.
    // #[yaserde(prefix = "nsb", rename = "EcpUrl-aggr")]
    // ecp_url_aggr: Option<String>,

    // /// The EcpUrl-extinstall element contains a value that, when appended to the value of the
    // /// EcpUrl element, results in a URL that can be used to view or change the mail addins
    // /// currently installed in the user’s mailbox.
    // #[yaserde(prefix = "nsb", rename = "EcpUrl-extinstall")]
    // ecp_url_extinstall: Option<String>,

    // /// The EcpUrl-mt element contains a value that, when appended to the value of the EcpUrl
    // /// element, results in a URL that can be used to access email message tracking settings.
    // #[yaserde(prefix = "nsb", rename = "EcpUrl-mt")]
    // ecp_url_mt: Option<String>,

    // /// The EcpUrl-photo element contains a value that, when appended to the value of the EcpUrl
    // /// element, results in a URL that can be used to view or change the user’s current photo.
    // #[yaserde(prefix = "nsb", rename = "EcpUrl-photo")]
    // ecp_url_photo: Option<String>,

    // /// The EcpUrl-publish element contains a value that, when appended to the value of the
    // /// EcpUrl element, results in a URL that can be used to access calendar publishing
    // /// settings.
    // #[yaserde(prefix = "nsb", rename = "EcpUrl-publish")]
    // ecp_url_publish: Option<String>,

    // /// The EcpUrl-ret element contains a value that, when appended to the value of the EcpUrl
    // /// element, results in a URL that can be used to access retention tag settings.
    // #[yaserde(prefix = "nsb", rename = "EcpUrl-ret")]
    // ecp_url_ret: Option<String>,

    // /// The EcpUrl-sms element contains a value that, when appended to the value of the EcpUrl
    // /// element, results in a URL that can be used to access Short Message Service (SMS)
    // /// settings.
    // #[yaserde(prefix = "nsb", rename = "EcpUrl-sms")]
    // ecp_url_sms: Option<String>,

    // /// The EcpUrl-tm element contains a value that, when appended to the value of the EcpUrl
    // /// element, results in a URL that can be used to access a list of all site mailboxes of
    // /// which the user is currently a member.
    // #[yaserde(prefix = "nsb", rename = "EcpUrl-tm")]
    // ecp_url_tm: Option<String>,

    // /// The EcpUrl-tmCreating element contains a value that, when appended to the value of the
    // /// EcpUrl element, results in a URL that can be used to create a new site mailbox.
    // #[yaserde(prefix = "nsb", rename = "EcpUrl-tmCreating")]
    // ecp_url_tm_creating: Option<String>,

    // /// The EcpUrl-tmEditing element contains a value that, when appended to the value of the
    // /// EcpUrl element, results in a URL that can be used to edit an existing site mailbox.
    // #[yaserde(prefix = "nsb", rename = "EcpUrl-tmEditing")]
    // ecp_url_tm_editing: Option<String>,

    // /// The EcpUrl-tmHiding element contains a value that, when appended to the value of the
    // /// EcpUrl element, results in a URL that can be used to unsubscribe the user from a site
    // /// mailbox.
    // #[yaserde(prefix = "nsb", rename = "EcpUrl-tmHiding")]
    // ecp_url_tm_hiding: Option<String>,

    // /// The EcpUrl-um element contains a value that, when appended to the value of the EcpUrl
    // /// element, results in a URL that can be used to access voice mail settings.
    // #[yaserde(prefix = "nsb", rename = "EcpUrl-um")]
    // ecp_url_um: Option<String>,

    /// The Encryption element specifies the required encryption for the connection to the server.
    /// This element is valid only if the value of the Type element is "IMAP, "POP3", or "SMTP". If
    /// the Encryption element is present, it overrides the SSL element.
    #[yaserde(prefix = "nsb", rename = "Encryption")]
    encryption: Option<Encryption>,

    /// The EmwsUrl element specifies the URL for the management web services virtual directory.
    #[yaserde(prefix = "nsb", rename = "EmwsUrl")]
    emws_url: Option<String>,

    /// The EwsUrl element specifies the URL for the web services virtual directory.
    #[yaserde(prefix = "nsb", rename = "EwsUrl")]
    ews_url: Option<String>,

    /// The External element contains the collection of URLs that a client can connect to outside
    /// the firewall.
    #[yaserde(prefix = "nsb", rename = "External")]
    external: Option<ConnInfo>,

    /// The GroupingInformation element specifies the grouping hint for certain clients.
    #[yaserde(prefix = "nsb", rename = "GroupingInformation")]
    grouping_information: Option<String>,

    /// The Internal element contains a collection of URLs that a client can connect to when it is
    /// inside the firewall.
    #[yaserde(prefix = "nsb", rename = "Internal")]
    internal: Option<ConnInfo>,

    /// The LoginName element specifies the user's mail server logon name.
    #[yaserde(prefix = "nsb", rename = "LoginName")]
    login_name: Option<String>,

    /// The MailStore element contains information that the client can use to connect to a mailbox
    /// via Messaging Application Programming Interface (MAPI) Extensions for HTTP, as specified in
    /// [MSOXCMAPIHTTP], to retrieve mailbox information.
    #[yaserde(prefix = "nsb", rename = "MailStore")]
    mail_store: Option<NSPIInfo>,

    /// The MdbDN element contains the DN of the mailbox database.
    #[yaserde(prefix = "nsb", rename = "MdbDN")]
    mdb_dn: Option<String>,

    /// The OABUrl element specifies the offline address book (OAB) configuration server URL for
    /// a server.
    #[yaserde(prefix = "nsb", rename = "OABUrl")]
    oab_url: Option<String>,

    /// The OOFUrl element specifies the URL of the best instance of the Out of Office (OOF) Web
    /// Service for a mail-enabled user.
    #[yaserde(prefix = "nsb", rename = "OOFUrl")]
    oof_url: Option<String>,

    /// The Port element specifies the port that is used to connect to the message store.
    #[yaserde(prefix = "nsb", rename = "Port")]
    port: Option<u16>,

    /// The PublicFolderServer element specifies the FQDN for the public folder server.
    #[yaserde(prefix = "nsb", rename = "PublicFolderServer")]
    public_folder_server: Option<String>,

    /// The ReferralPort element specifies the port that is used to get a referral to a directory.
    #[yaserde(prefix = "nsb", rename = "ReferralPort")]
    referral_port: Option<u16>,

    /// The Server element specifies the name of the mail server. It is a required child element of
    /// the Protocol element that has a Type element value of "EXCH", "EXPR", "EXHTTP", "POP3",
    /// "SMTP", or "IMAP". The value will be either a host name or an IP address.
    #[yaserde(prefix = "nsb", rename = "Server")]
    server: Option<String>,

    /// The ServerDN element specifies the DN of the mail server. It is a required child element of
    /// the Protocol element when the Type element has a value of "EXCH".
    #[yaserde(prefix = "nsb", rename = "ServerDN")]
    server_dn: Option<String>,

    /// The ServerExclusiveConnect element specifies whether the client uses the connection
    /// information contained in the parent Protocol element first when the client attempts to
    /// connect to the server.
    ///
    /// The ServerExclusiveConnect element is used only when the Type element is equal to "EXPR",
    /// "EXCH", or "EXHTTP".
    #[yaserde(prefix = "nsb", rename = "ServerExclusiveConnect")]
    server_exclusive_connect: Option<Switch>,

    /// The ServerVersion element represents the version number of the server software. It is an
    /// optional child element of the Protocol element.
    ///
    /// The ServerVersion value is a 32-bit hexadecimal number that contains the major version
    /// number, minor version number, and major build number of the server. The ServerVersion
    /// element is used only when the Type element has a value of "EXCH".
    #[yaserde(prefix = "nsb", rename = "ServerVersion")]
    server_version: Option<String>,

    /// The SharingUrl element specifies the endpoint for a sharing server, which is a server used
    /// for sharing calendars and contacts.
    #[yaserde(prefix = "nsb", rename = "SharingUrl")]
    sharing_url: Option<String>,

    /// The SiteMailboxCreationURL element contains a URL to a self-service web site that can be
    /// used to create a new site mailbox.
    #[yaserde(prefix = "nsb", rename = "SiteMailboxCreationURL")]
    site_mailbox_creation_url: Option<String>,

    /// The SMTPLast element specifies whether the Simple Mail Transfer Protocol (SMTP) server
    /// requires that email be downloaded before it sends email by using the SMTP server.
    #[yaserde(prefix = "nsb", rename = "SMTPLast")]
    smtp_last: Option<Switch>,

    /// The SPA element indicates whether secure password authentication is required. This element
    /// is only valid when the value of the Type element is "SMTP", "POP3", or "IMAP".
    #[yaserde(prefix = "nsb", rename = "SPA")]
    spa: Option<Switch>,

    /// The SSL element specifies whether the server requires SSL for logon.
    #[yaserde(prefix = "nsb", rename = "SSL")]
    ssl: Option<Switch>,

    /// The TTL element specifies the time, in hours, during which the settings remain valid.
    #[yaserde(prefix = "nsb", rename = "TTL")]
    ttl: Option<u32>,

    // TODO: support this as a child?
    /// The Type element identifies the type of the configured mail account. It is an optional
    /// child element of the Protocol element. If the Protocol element has a Type attribute, then
    /// the Type element MUST NOT be present. If the Protocol element does not have a Type
    /// attribute, then the Type element MUST be present.
    #[yaserde(prefix = "nsb", rename = "type", attribute)]
    _type: ProtocolType,

    /// The UMUrl element specifies the URL of the best instance of the Voice Mail Settings Web
    /// Service protocol ([MS-OXWUMS]) for a mail-enabled user.
    #[yaserde(prefix = "nsb", rename = "UMUrl")]
    um_url: Option<String>,

    /// The UsePOPAuth element indicates whether the authentication information that is provided
    /// for a POP3 type of account is also used for SMTP.
    #[yaserde(prefix = "nsb", rename = "UsePOPAuth")]
    use_pop_auth: Option<Switch>,

    /// An integer value that MUST be greater than zero (0) and less than or equal to the value of
    /// the XMapiHttpCapability header included in the Autodiscover request.
    #[yaserde(attribute, prefix = "nsb", rename = "Version")]
    version: Option<i32>,
}

impl Protocol {
    fn with_type(mut self, protocol_type: ProtocolType) -> Self {
        self._type = protocol_type;
        self
    }

    fn with_misc(mut self) -> Self {
        self.ttl = Some(1);
        self.encryption = Some(Encryption::TLS);
        self
    }

    fn fill_encryption(mut self) -> Self {
        self.encryption = match (&self._type, &self.port) {
            (ProtocolType::IMAP, Some(143)) => Some(Encryption::TLS),
            (ProtocolType::IMAP, Some(993)) => Some(Encryption::SSL),
            (ProtocolType::SMTP, Some(25)) =>  Some(Encryption::TLS),
            (ProtocolType::SMTP, Some(465)) => Some(Encryption::SSL),
            (ProtocolType::SMTP, Some(587)) => Some(Encryption::TLS),
            (_, _) => None,
        };

        if self.encryption == Some(Encryption::SSL) {
            self.ssl = Some(Switch::On);
        }
        self
    }

    fn with_server(mut self, server: String, port: u16) -> Self {
        self.server = Some(server);
        self.port = Some(port);
        self
    }

    fn with_domain(mut self, domain: String) -> Self {
        self.domain_name = Some(domain);
        self.domain_required = Some(Switch::On);
        self
    }

    fn with_login(mut self, login: String) -> Self {
        self.login_name = Some(login);
        self.auth_required = Some(Switch::On);
        self.spa = Some(Switch::On);
        self
    }
}

#[derive(Debug, YaDeserialize, YaSerialize, Default)]
#[yaserde(
    namespace = "nsb: http://schemas.microsoft.com/exchange/autodiscover/outlook/responseschema/2006a",
    prefix = "nsb",
    default_namespace = "nsb",
)]
struct NSPIInfo {

    /// The ExternalUrl element contains a URL that the client can use to connect to an NSPI server
    /// via Messaging Application Programming Interface (MAPI) Extensions for HTTP when the client
    /// is located outside of the firewall.
    #[yaserde(prefix = "nsb", rename = "ExternalUrl")]
    external_url: Option<String>,

    /// The InternalUrl element contains a URL that the client can use to connect to an NSPI server
    /// via Messaging Application Programming Interface (MAPI) Extensions for HTTP when the client
    /// is located inside of the firewall.
    #[yaserde(prefix = "nsb", rename = "InternalUrl")]
    internal_url: Option<String>,
}

#[derive(Debug, YaDeserialize, YaSerialize, Default)]
#[yaserde(
    namespace = "nsb: http://schemas.microsoft.com/exchange/autodiscover/outlook/responseschema/2006a",
    prefix = "nsb",
    default_namespace = "nsb",
)]
struct ConnInfo {
    #[yaserde(prefix = "nsb", rename = "OWAUrl")]
    owa_urls: Vec<OWAUrl>,

    #[yaserde(prefix = "nsb", rename = "Protocol")]
    protocol: ConnProtocol,
}

#[derive(Debug, YaDeserialize, YaSerialize, Default)]
#[yaserde(
    namespace = "nsb: http://schemas.microsoft.com/exchange/autodiscover/outlook/responseschema/2006a",
    prefix = "nsb",
    default_namespace = "nsb",
)]
struct OWAUrl {
    #[yaserde(text)]
    url: String,

    #[yaserde(attribute, prefix = "nsb", rename = "AuthenticationMethod")]
    authentication_method: String,
}

#[derive(Debug, YaDeserialize, YaSerialize, Default)]
#[yaserde(
    namespace = "nsb: http://schemas.microsoft.com/exchange/autodiscover/outlook/responseschema/2006a",
    prefix = "nsb",
    default_namespace = "nsb",
)]
struct ConnProtocol {
    #[yaserde(prefix = "nsb", rename = "Type")]
    _type: ProtocolType,

    #[yaserde(prefix = "nsb", rename = "ASUrl")]
    as_url: String,
}

/// The Type element identifies the type of the additional mail account.
#[derive(Debug, YaDeserialize, YaSerialize)]
enum AlternativeMailboxType {
    /// The alternative mailbox represented by the parent AlternativeMailbox element is an archive
    /// mailbox for the user. An archive mailbox is a second mailbox provisioned for a user that is
    /// used to store historical messaging data.
    Archive,

    /// The alternative mailbox represented by the parent AlternativeMailbox element is owned by
    /// another user. The current user has permissions to open this mailbox.
    Delegate,

    /// The alternative mailbox represented by the parent AlternativeMailbox element is a site
    /// mailbox that is configured for the user.
    TeamMailbox,
}

impl Default for AlternativeMailboxType {
    fn default() -> Self {
        Self::Archive
    }
}

#[derive(Debug, YaDeserialize, YaSerialize, Default)]
#[yaserde(
    root = "AlternativeMailbox"
    namespace = "nsb: http://schemas.microsoft.com/exchange/autodiscover/outlook/responseschema/2006a",
    prefix = "nsb",
    default_namespace = "nsb",
)]
struct AlternativeMailbox {
    /// The DisplayName element represents the additional mailbox user's display name.
    #[yaserde(prefix = "nsb", rename = "DisplayName")]
    display_name: String,

    /// The LegacyDN element identifies the additional mailbox by DN.
    #[yaserde(prefix = "nsb", rename = "LegacyDN")]
    legacy_dn: Option<String>,

    /// The Server element contains the FQDN of the mail server that contains the additional
    /// mailbox.
    #[yaserde(prefix = "nsb", rename = "Server")]
    server: Option<String>,

    /// The SmtpAddress element contains an SMTP address assigned to the alternative mailbox. This
    /// SMTP address can be used in the EMailAddress element of an Autodiscover request to discover
    /// configuration settings for the alternative mailbox.
    #[yaserde(prefix = "nsb", rename = "SmtpAddress")]
    smtp_address: Option<String>,

    /// The Type element identifies the type of the additional mail account.
    #[yaserde(prefix = "nsb", rename = "Type")]
    _type: AlternativeMailboxType,
}

#[derive(Debug, YaDeserialize, YaSerialize, Default)]
#[yaserde(
    root = "PublicFolderInformation"
    namespace = "nsb: http://schemas.microsoft.com/exchange/autodiscover/outlook/responseschema/2006a",
    prefix = "nsb",
    default_namespace = "nsb",
)]
struct PublicFolderInformation {
    #[yaserde(prefix = "nsb", rename = "SmtpAddress")]
    smtp_address: String,
}


#[derive(Debug, YaDeserialize, YaSerialize, Default)]
#[yaserde(
    root = "Autodiscover",
    namespace = "ns: http://schemas.microsoft.com/exchange/autodiscover/outlook/requestschema/2006",
    prefix = "ns",
    default_namespace = "ns",
)]
pub struct AutodiscoverRequest {
    #[yaserde(rename = "Request")]
    request: AutodiscoverRequestBody,
}

impl AutodiscoverRequest {
    pub fn email(self) -> Option<String> {
        self.request.email_address
    }

    pub fn legacy_dn(self) -> Option<String> {
        self.request.legacy_dn
    }

    pub fn with_email(mut self, email: String) -> Self {
        self.request.email_address = Some(email);
        self
    }

    pub fn with_legacy_dn(mut self, legacy_dn: String) -> Self {
        self.request.legacy_dn = Some(legacy_dn);
        self
    }
}

#[derive(Debug, YaDeserialize, YaSerialize, Default)]
#[yaserde(
    root = "Request",
    namespace = "ns: http://schemas.microsoft.com/exchange/autodiscover/outlook/requestschema/2006",
    prefix = "ns",
    default_namespace = "ns",
)]
struct AutodiscoverRequestBody {
    #[yaserde(rename = "EMailAddress", prefix = "ns")]
    email_address: Option<String>,

    #[yaserde(rename = "LegacyDN", prefix = "ns")]
    legacy_dn: Option<String>,

    #[yaserde(rename = "AcceptableResponseSchema", prefix = "ns")]
    acceptable_response_schema: AcceptableResponseSchema,
}

#[derive(Debug, YaDeserialize, YaSerialize)]
enum AcceptableResponseSchema {
    #[yaserde(rename = "http://schemas.microsoft.com/exchange/autodiscover/responseschema/2006")]
    Schema2006a,
}

impl Default for AcceptableResponseSchema {
    fn default() -> Self {
        Self::Schema2006a
    }
}
